var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var users = require('./routes/users');


var graphqlHTTP = require('express-graphql');
var { buildSchema } = require('graphql');

const fs = require('fs')


var schema = buildSchema(fs.readFileSync('./graphql/schema.graphql',{encoding: 'utf-8'}));


var courseData = [
  {
    id: 1,
    title: 'The Complete Node.js Developer Course',
    author: 'Andrew'    
  },
  
  {
    id: 2,
    title: 'C# Programming Language',
    author: 'James'
  }
]

var getCourse = function(args) {
  var id = args.id;
  return courseData.filter(course => {
    return course.id == id; 
  })[0];
}

var root = { 
  course: getCourse, 
};



var app = express();

app.use('/graphql/users', graphqlHTTP({
    schema: schema,
    rootValue: root,
    graphiql: true,
  }));
  /*
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser())

app.use('/api/v1/users', users);
*/

// DynamoDB Test

var AWS = require("aws-sdk");

AWS.config.update({
  region: "ap-southeast-1",
  endpoint: "https://dynamodb.ap-southeast-1.amazonaws.com"
});

var dynamodb = new AWS.DynamoDB();

var params = {
    TableName : "Movies",
    KeySchema: [       
        { AttributeName: "year", KeyType: "HASH"},  //Partition key
        { AttributeName: "title", KeyType: "RANGE" }  //Sort key
    ],
    AttributeDefinitions: [       
        { AttributeName: "year", AttributeType: "N" },
        { AttributeName: "title", AttributeType: "S" }
    ],
    ProvisionedThroughput: {       
        ReadCapacityUnits: 10, 
        WriteCapacityUnits: 10
    }
};


dynamodb.updateTable(params, function(err, data) {
  if (err) {
      console.error("Unable to create table. Error JSON:", JSON.stringify(err, null, 2));
  } else {
      console.log("Created table. Table description JSON:", JSON.stringify(data, null, 2));
  }
});




module.exports = app;
