var root = { 
    course: getCourse, 
};

var courseData = [
  {
    id: 1,
    title: 'The Complete Node.js Developer Course',
    author: 'Andrew'    
  },
  
  {
    id: 2,
    title: 'C# Programming Language',
    author: 'James'
  }
]

var getCourse = function(args) {
  var id = args.id;
  return courseData.filter(course => {
    return course.id == id; 
  })[0];
}
  
module.exports = {root}